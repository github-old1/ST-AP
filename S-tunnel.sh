#!/bin/bash

### Variables ###
# Change this section #
sshUser='mparvin' # ssh User to login in remote server
sshKey='~/.ssh/id_rsa' # Use this key to authenticate with server
sshIP='10.10.10.10' # Remote server IP or Hostname
proxyPort='8080'
######################


ssh -i $sshKey -D $proxyPort -N $sshUser@$sshIP
