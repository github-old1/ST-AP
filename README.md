# ST-AP
SSH Tunnel as proxy - with supervisor config

Every day and every time I use my computer, I must to use a proxy for using some services that **filtered(blocked)** in my country.

So I wrote this script(S-tunnel.sh) and used the supervisor to run a ssh tunnel every time.

This script uses ssh command to run ssh tunnel and ssh key to login in remote server.
Before run the script, please copy your public key to remote server.

If you want to use this script, Please change files accourding to this guide:

Copy S-tunnel.sh for to your scripts directory and copy sshTunnel.conf to /etc/supervisor/conf.d folder, then
edit S-tunnel.sh file and **only** change Variables section:

#### Edit S-tunnel.sh

put your username in remote server in sshUser, attention this user must be exists on remote server 
and your public key must be added to this users authorized_keys file
```
sshUser='mparvin' 
```

Your private key path
```
sshKey='~/.ssh/id_rsa'
```

Remote server IP or Hostname
```
sshIP='10.10.10.10'
```

Use this port in your program as Socks5 port
```
proxyPort='8080'
```

#### Edit sshTunnel.conf

Full path to S-Tunnel scripts
```command=/PATH-TO-SCRIPTS-FOLDER/S-tunnel.sh       ```

Path to scripts folder(which contains S-Tunnel script)
```directory=/PATH-TO-SCRIPTS-FOLDER                ```

Who wants to run this tunnel, attention: this user ssh-key must added to remote server
```user=mparvin                                      ```


After your changes finished run this command to update your supervisor config:
```
supervisorctl reread
supervisorctl update
```

To see your supervisor status run this command:
```
supervisorctl status
```

For debug and find errors read this file:
```
/var/log/supervisor/sshtunnel.log
```
